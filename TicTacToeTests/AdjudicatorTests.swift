//
//  AdjudicatorTests.swift
//  TicTacToeTests
//
//  Created by Keith Dev on 06/03/2020.
//  Copyright © 2020 Founders4Schools. All rights reserved.
//

import XCTest
@testable import TicTacToe

class AdjudicatorTests: XCTestCase {
    
    func makeSUT() -> Adjudicator {
        let board = Board()
        let sut = Adjudicator(board: board)
        return sut
    }

    func test_init(){
        let sut = makeSUT()
        XCTAssertNotNil(sut)
    }
    
    func test_Adjudicator_has_Board(){
        let sut = makeSUT()
        XCTAssertNotNil(sut.board)
        
    }
    
    func test_Adjudicator_is_game_won_when_board_empty(){
        let sut = makeSUT()
        XCTAssertFalse(sut.isGameWon())
    }
    
    func test_Adjudicator_is_game_won_with_three_crosses_in_first_row(){
        let sut = makeSUT()
        sut.board.recordMove(row: 0, column: 0, value: SquareValue.cross)
        sut.board.recordMove(row: 0, column: 1, value: SquareValue.cross)
        sut.board.recordMove(row: 0, column: 2, value: SquareValue.cross)

        XCTAssertTrue(sut.isGameWon())
    }
    
    func test_Adjudicator_is_game_won_with_two_crosses_in_first_row(){
        let sut = makeSUT()
        sut.board.recordMove(row: 0, column: 0, value: SquareValue.cross)
        sut.board.recordMove(row: 0, column: 1, value: SquareValue.cross)
        sut.board.recordMove(row: 0, column: 2, value: SquareValue.nought)

        XCTAssertFalse(sut.isGameWon())
    }
    
    func test_Adjudicator_is_game_won_with_three_crosses_in_second_row(){
        let sut = makeSUT()
        sut.board.recordMove(row: 1, column: 0, value: SquareValue.cross)
        sut.board.recordMove(row: 1, column: 1, value: SquareValue.cross)
        sut.board.recordMove(row: 1, column: 2, value: SquareValue.cross)

        XCTAssertTrue(sut.isGameWon())
    }
    
    func test_Adjudicator_is_game_won_with_three_crosses_in_second_column(){
        let sut = makeSUT()
        sut.board.recordMove(row: 0, column: 1, value: SquareValue.cross)
        sut.board.recordMove(row: 1, column: 1, value: SquareValue.cross)
        sut.board.recordMove(row: 2, column: 1, value: SquareValue.cross)

        XCTAssertTrue(sut.isGameWon())
    }
    
    func test_Adjudicator_is_game_won_with_three_crosses_in_leading_diagonal(){
         let sut = makeSUT()
         sut.board.recordMove(row: 0, column: 0, value: SquareValue.cross)
         sut.board.recordMove(row: 1, column: 1, value: SquareValue.cross)
         sut.board.recordMove(row: 2, column: 2, value: SquareValue.cross)

         XCTAssertTrue(sut.isGameWon())
     }
    
    func test_Adjudicator_is_game_won_with_three_crosses_in_trailing_diagonal(){
         let sut = makeSUT()
         sut.board.recordMove(row: 0, column: 2, value: SquareValue.cross)
         sut.board.recordMove(row: 1, column: 1, value: SquareValue.cross)
         sut.board.recordMove(row: 2, column: 0, value: SquareValue.cross)

         XCTAssertTrue(sut.isGameWon())
     }

}
