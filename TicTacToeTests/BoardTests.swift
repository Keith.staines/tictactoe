//
//  TicTacToeTests.swift
//  TicTacToeTests
//
//  Created by Keith Dev on 14/02/2020.
//  Copyright © 2020 Founders4Schools. All rights reserved.
//

import XCTest
@testable import TicTacToe

class BoardTests: XCTestCase {
    
    func makeSUT() -> Board {
        let board: Board
        board = Board()
        return board
    }

    func test_instatiate_board() {
        let sut = makeSUT()
        XCTAssertNotNil(sut)
    }
    
    func test_board_has_cells_collection() {
        let sut = makeSUT()
        XCTAssertNotNil(sut.cells)
    }
    
    func test_board_has_nine_cells() {
        let sut = makeSUT()
        XCTAssertEqual(sut.cells.count, 9)
    }
    
    func test_board_is_empty(){
        let sut = makeSUT()
        XCTAssertTrue(sut.isEmpty())
    }
    
    func test_board_record_move(){
        let sut = makeSUT()
        sut.recordMove(index:3,value:SquareValue.cross)
        XCTAssertFalse(sut.isEmpty())
    }
    
    func test_board_record_move_by_row_and_column(){
        let sut = makeSUT()
        sut.recordMove(row: 1, column: 2, value: SquareValue.cross)
        XCTAssertEqual(sut.cells[5], SquareValue.cross)
        
    }
    
    func test_board_get_square_state(){
        let sut = makeSUT()
        sut.recordMove(row: 1, column: 2, value: SquareValue.cross)
        let state = sut.getSquareState(row: 1, column: 2)
        XCTAssertEqual(state, SquareValue.cross)
    }
    
    func test_board_convertRowAndColumnToIndex(){
        let sut = makeSUT()
        let indexA = sut.convertRowAndColumnToIndex(row:0, column:2)
        let indexB = sut.convertRowAndColumnToIndex(row:1, column:0)
        let indexC = sut.convertRowAndColumnToIndex(row:2, column:2)
        XCTAssertEqual(indexA,2)
        XCTAssertEqual(indexB,3)
        XCTAssertEqual(indexC,8)
    }
    
    func test_board_get_row_state(){
        let sut = makeSUT()
        sut.recordMove(row: 0, column: 0, value: SquareValue.cross)
        sut.recordMove(row: 0, column: 1, value: SquareValue.nought)
        let row = sut.getRowState(row: 0)
        XCTAssertEqual(row, [SquareValue.cross,SquareValue.nought,SquareValue.empty,])
    }
    
    func test_board_get_column_state(){
        let sut = makeSUT()
        sut.recordMove(row: 0, column: 1, value: SquareValue.cross)
        sut.recordMove(row: 2, column: 1, value: SquareValue.nought)
        let column = sut.getColumnState(column: 1)
        XCTAssertEqual(column, [SquareValue.cross,SquareValue.empty,SquareValue.nought,])
    }
    
    func test_board_get_leading_diagonal_state(){
        let sut = makeSUT()
        sut.recordMove(row: 0, column: 0, value: SquareValue.cross)
        sut.recordMove(row: 1, column: 1, value: SquareValue.nought)
        let leadingDiagonal = sut.getLeadingDiagonalState()
        XCTAssertEqual(leadingDiagonal, [SquareValue.cross,SquareValue.nought,SquareValue.empty,])
    }
    
    func test_board_get_trailing_diagonal_state(){
        let sut = makeSUT()
        sut.recordMove(row: 1, column: 1, value: SquareValue.cross)
        sut.recordMove(row: 2, column: 0, value: SquareValue.nought)
        let trailingDiagonal = sut.getTrailingDiagonalState()
        XCTAssertEqual(trailingDiagonal, [SquareValue.empty,SquareValue.cross,SquareValue.nought,])
    }

}
