//
//  Adjudicator.swift
//  TicTacToe
//
//  Created by Keith Dev on 06/03/2020.
//  Copyright © 2020 Founders4Schools. All rights reserved.
//

import Foundation

public protocol GameAdjudicating: WinDetecting, MoveValidating {

}

public protocol WinDetecting{
    func isGameWon() -> Bool
}

public protocol MoveValidating{
    func isValidMove(row:Int, column:Int, value:Board.SquareState) -> Bool
}

public class Adjudicator: GameAdjudicating {
    
    let board : Board
    let winDetector:WinDetecting
    let moveValidator:MoveValidating
    public init(board: Board) {
        self.board = board
        self.winDetector = WinDetector(board:self.board)
        self.moveValidator = MoveValidator()
    }
    
    public func isGameWon() -> Bool {
        return self.winDetector.isGameWon()
    }
    
    public func isValidMove(row:Int, column:Int, value:Board.SquareState) -> Bool{
        return self.moveValidator.isValidMove(row: row, column: column, value: value)
    }
}


public class MoveValidator: MoveValidating{
    public func isValidMove(row: Int, column: Int, value: Board.SquareState) -> Bool {
        return true
    }
    
    
}


public class WinDetector: WinDetecting{
    
    let board : Board
    
    public init(board: Board) {
        self.board = board
    }
    
    public func isGameWon() -> Bool {
        for index in 0..<3{
            if isWinningRow(index) {return true}
            if isWinningColumn(index){return true}
        }
        if isWinningLeadingDiagonal(){return true}
        if isWinningTrailingDiagonal(){return true}
        return false
    }
    
    func isWinningRow(_ row:Int) -> Bool{
        let rowStates = board.getRowState(row: row)
        return isWinningArray(rowStates)
    }
    
    func isWinningColumn(_ column:Int) -> Bool{
        let columnStates = board.getColumnState(column: column)
        return isWinningArray(columnStates)
    }
    
    func isWinningLeadingDiagonal() -> Bool{
        let leadingDiagonalStates = board.getLeadingDiagonalState()
        return isWinningArray(leadingDiagonalStates)
    }
    
    func isWinningTrailingDiagonal() -> Bool{
        let trailingDiagonalStates = board.getTrailingDiagonalState()
        return isWinningArray(trailingDiagonalStates)
    }
    
    func isWinningArray(_ array:[SquareValue]) -> Bool{
        let firstValue = array[0]
        if firstValue == .empty{
            return false
        }
        for value in array {
            if value != firstValue {
                return false
            }
        }
        return true
    }
}
