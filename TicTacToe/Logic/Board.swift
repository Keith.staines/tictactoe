//
//  Board.swift
//  TicTacToe
//
//  Created by Keith Dev on 14/02/2020.
//  Copyright © 2020 Founders4Schools. All rights reserved.
//

import Foundation

public typealias SquareValue = Board.SquareState

public class Board {
    public enum SquareState: Int {
        case empty
        case cross
        case nought
    }
    
    /* Row and Column numbers vs (linearr index)
               Col0   Col    Co2
        Row0   0(0)   1(1)   2(2)
        Row1   0(3)   1(4)   2(5)
        Row2   0(6)   1(7)   2(8)
     */
    
    var cells: [SquareState] = [.empty,.empty,.empty,
                                .empty,.empty,.empty,
                                .empty,.empty,.empty]
    
    func isEmpty() -> Bool {
        let nonZeroes = cells.filter { (val) -> Bool in
            val != .empty
        }
        return nonZeroes.isEmpty
    }
    
    func recordMove(index:Int,value:SquareState){
        cells[index] = value
    }
    
    func recordMove(row:Int, column:Int, value:SquareState){
        let index = convertRowAndColumnToIndex(row: row, column: column)
        recordMove(index: index, value: value)
    }
    
    func convertRowAndColumnToIndex(row: Int, column: Int) -> Int{
        return row*3 + column
    }
    
    func getSquareState(row: Int, column: Int) -> SquareState{
        let index = convertRowAndColumnToIndex(row: row, column: column)
        return cells[index]
    }
    
    func getRowState(row: Int) -> [SquareState]{
        let value1 = getSquareState(row: row, column: 0)
        let value2 = getSquareState(row: row, column: 1)
        let value3 = getSquareState(row: row, column: 2)
        
        return [value1,value2,value3]
    }
    
    func getColumnState(column: Int) -> [SquareState]{
        let value1 = getSquareState(row: 0, column: column)
        let value2 = getSquareState(row: 1, column: column)
        let value3 = getSquareState(row: 2, column: column)
        
        return [value1,value2,value3]
    }
    
    func getLeadingDiagonalState() -> [SquareState]{
        let value1 = getSquareState(row: 0, column: 0)
        let value2 = getSquareState(row: 1, column: 1)
        let value3 = getSquareState(row: 2, column: 2)
        
        return [value1,value2,value3]
        
    }
    
    func getTrailingDiagonalState() -> [SquareState]{
        let value1 = getSquareState(row: 0, column: 2)
        let value2 = getSquareState(row: 1, column: 1)
        let value3 = getSquareState(row: 2, column: 0)
        
        return [value1,value2,value3]
        
    }
    
    
}
